#### Task
A simple system for online room reservations. The system is used by hotel owners
and customers. Customers can book the room. Hotel owners can define which rooms are available.
Operations available in the system

1. Register a customer.
2. User searches for available hotel rooms. Search criteria include: period, city,daily price range.
3. User asks for room reservation for specific period.
4. User can check their reservations.
5. User can cancel his reservation.

Hotel and its room configuration should be pre-configured, there is no need to expose any endpoint to
add/change/remove hotel nor room in hotel. There are no other business requirements than the ones
described above. We will be assessing technical aspects of the solution, so please don�t implement
additional requirements.

#### Description of solution
  
For the simplicity of the project it is based on H2 memory database. Here is the listing of most useful maven
commands:

* mvn clean spring-boot:run (runs the app with clean database)
* mvn clean spring-boot:run -Dspring-boot.run.profiles=dev (runs the app with some sample hotel data)
* mvn clean test (runs acceptance tests)

#### Useful endpoints
* http://localhost:8080/console (H2 console)
* http://localhost:8080/swagger-ui.html (Swagger - REST UI doc)


 



