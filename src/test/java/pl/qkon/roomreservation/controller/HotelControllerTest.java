package pl.qkon.roomreservation.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.qkon.roomreservation.dictionary.City;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HotelControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void findRoom_errorDatesInThePast() throws Exception {

        mockMvc.perform(
                get("/api/room")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("city", City.WARSAW.name())
                        .param("dateFrom", "2018-01-01")
                        .param("dateTo", "2018-02-01")
                        .param("priceFrom", "50")
                        .param("priceTo", "150"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void findRoom_errorDatesFromAndToMixed() throws Exception {
        mockMvc.perform(
                get("/api/room")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("city", City.WARSAW.name())
                        .param("dateFrom", "2018-03-01")
                        .param("dateTo", "2018-02-01")
                        .param("priceFrom", "50")
                        .param("priceTo", "150")
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void findRoom_errorDatesInTheSameDay() throws Exception {
        mockMvc.perform(
                get("/api/room")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("city", City.WARSAW.name())
                        .param("dateFrom", "2018-02-01")
                        .param("dateTo", "2018-02-01")
                        .param("priceFrom", "50")
                        .param("priceTo", "150")
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void findRoom_errorPricesRangeIncorrect() throws Exception {
        mockMvc.perform(
                get("/api/room")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("city", City.WARSAW.name())
                        .param("dateFrom", LocalDate.of(2030, 10, 20).toString())
                        .param("dateTo", LocalDate.of(2030, 11, 20).toString())
                        .param("priceFrom", "150")
                        .param("priceTo", "50")
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void findRoom_successful() throws Exception {
        mockMvc.perform(
                get("/api/room")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("city", City.WARSAW.name())
                        .param("dateFrom", LocalDate.of(2030, 10, 20).toString())
                        .param("dateTo", LocalDate.of(2030, 11, 20).toString())
                        .param("priceFrom", "50")
                        .param("priceTo", "150")
        )
                .andDo(print())
                .andExpect(status().isOk());
    }
}