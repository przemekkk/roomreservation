package pl.qkon.roomreservation.controller;

import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.qkon.roomreservation.processor.command.RegisterCustomerCommand;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest {

    public static final Gson GSON = new Gson();
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testNoUsersInTheSystem() throws Exception {
        mockMvc.perform(get("/api/customer"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testCreationOfANewUser() throws Exception {
        RegisterCustomerCommand command = RegisterCustomerCommand.builder()
                .name("P")
                .lastName("K")
                .build();
        String json = GSON.toJson(command);

        mockMvc.perform(
                post("/api/customer")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isCreated());
    }
}