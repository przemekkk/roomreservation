package pl.qkon.roomreservation.controller;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import pl.qkon.roomreservation.dictionary.City;
import pl.qkon.roomreservation.dictionary.ReservationStatus;
import pl.qkon.roomreservation.entity.Customer;
import pl.qkon.roomreservation.entity.Hotel;
import pl.qkon.roomreservation.entity.Reservation;
import pl.qkon.roomreservation.entity.Room;
import pl.qkon.roomreservation.processor.command.CancelReservationCommand;
import pl.qkon.roomreservation.processor.command.MakeReservationCommand;
import pl.qkon.roomreservation.repository.CustomerRepository;
import pl.qkon.roomreservation.repository.HotelRepository;
import pl.qkon.roomreservation.repository.RoomRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ReservationControllerTest {
    public static final Gson GSON = new Gson();

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private ReservationController reservationController;
    @Autowired
    private HotelController hotelController;

    private Customer customer;
    private Room room;


    @PostConstruct
    public void init() {
        customer = createCustomer();
        createHotel("HotelA", City.WARSAW);
        room = roomRepository.findAll()
                .stream()
                .findAny()
                .orElseThrow(IllegalStateException::new);
    }


    @Test
    public void makeReservation() throws Exception {

        String dateFrom = "2019-01-02";
        String dateTo = "2019-01-04";
        String dateBefore = "2019-01-01";
        String dateIn = "2019-01-03";
        String dateAfter = "2019-01-05";

        Assert.assertTrue(isRoomAvailable(dateFrom, dateTo));
        String command1 = buildMakeReservationCommand(room, dateFrom, dateTo);
        performMakeReservationCommand(command1, status().isOk());
        //make the same reservation second time
        Assert.assertFalse(isRoomAvailable(dateFrom, dateTo));
        performMakeReservationCommand(command1, status().is4xxClientError());

        //overlapping reservation
        String command2 = buildMakeReservationCommand(room, dateBefore, dateIn);
        Assert.assertFalse(isRoomAvailable(dateBefore, dateIn));
        performMakeReservationCommand(command2, status().is4xxClientError());
        //overlapping reservation
        String command3 = buildMakeReservationCommand(room, dateIn, dateAfter);
        Assert.assertFalse(isRoomAvailable(dateIn, dateAfter));
        performMakeReservationCommand(command3, status().is4xxClientError());

        //reservation just next to
        String command4 = buildMakeReservationCommand(room, dateTo, dateAfter);
        Assert.assertTrue(isRoomAvailable(dateTo, dateAfter));
        performMakeReservationCommand(command4, status().isOk());
        //reservation just next to
        String command5 = buildMakeReservationCommand(room, dateBefore, dateFrom);
        Assert.assertTrue(isRoomAvailable(dateBefore, dateFrom));
        performMakeReservationCommand(command5, status().isOk());


    }

    private boolean isRoomAvailable(String dateFrom, String dateTo) {
        return hotelController.findRoom(room.getHotel().getCity(), LocalDate.parse(dateFrom), LocalDate.parse(dateTo), room.getPrice(), room.getPrice())
                .stream()
                .anyMatch(r -> room.getId().equals(r.getId()));
    }

    @Test
    public void cancelReservation() throws Exception {


        MakeReservationCommand makeReservationCommand = MakeReservationCommand.builder()
                .dateFrom("2020-01-02")
                .dateTo("2020-01-04")
                .roomId(room.getId())
                .userId(customer.getId()).build();

        Reservation reservation = reservationController.makeReservation(makeReservationCommand);
        String cancelReservationCommand = GSON.toJson(CancelReservationCommand.builder()
                .reservationId(reservation.getId())
                .userId(customer.getId())
                .build());
        mockMvc.perform(
                put("/api/reservation")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(cancelReservationCommand))
                .andExpect(status().isOk()).andReturn();
        Assert.assertEquals(1, reservationController.findUserReservations(customer.getId()).stream().filter(res -> res.getReservationStatus().equals(ReservationStatus.CANCELLED))
                .count());
    }

    @Test
    public void findUserReservations() throws Exception {
        mockMvc.perform(
                get("/api/reservation")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("userId", customer.getId().toString()))
                .andExpect(status().isOk());
    }

    private String buildMakeReservationCommand(Room room, String s, String s2) {
        return GSON.toJson(MakeReservationCommand.builder()
                .dateFrom(s)
                .dateTo(s2)
                .roomId(room.getId())
                .userId(customer.getId()).build());
    }

    private MvcResult performMakeReservationCommand(String command3, ResultMatcher xxClientError) throws Exception {
        return mockMvc.perform(
                post("/api/reservation")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(command3))
                .andExpect(xxClientError).andReturn();
    }

    private Customer createCustomer() {
        return customerRepository.save(Customer.builder().name("A").lastName("B").build());
    }

    private Hotel createHotel(String name, City city) {
        Hotel hotel = Hotel.builder()
                .name(name)
                .city(city)
                .build();
        hotelRepository.save(hotel);
        createRoom(hotel, 50.00);
        createRoom(hotel, 100.00);
        return hotel;
    }

    private void createRoom(Hotel hotel, double v) {
        Room room = Room.builder()
                .hotel(hotel)
                .price(v)
                .build();
        roomRepository.save(room);
    }
}