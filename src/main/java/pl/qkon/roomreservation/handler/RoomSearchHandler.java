package pl.qkon.roomreservation.handler;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.qkon.roomreservation.dictionary.ReservationStatus;
import pl.qkon.roomreservation.entity.Reservation;
import pl.qkon.roomreservation.entity.Room;
import pl.qkon.roomreservation.exception.DateParamErrorException;
import pl.qkon.roomreservation.exception.WrongPriceRangeException;
import pl.qkon.roomreservation.handler.params.RoomSearchParams;
import pl.qkon.roomreservation.repository.ReservationRepository;
import pl.qkon.roomreservation.repository.RoomRepository;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class RoomSearchHandler {

    private final RoomRepository roomRepository;
    private final ReservationRepository reservationRepository;

    public static void validateDates(@NotNull LocalDate dateFrom, @NotNull LocalDate dateTo) {
        if (dateFrom.isBefore(LocalDate.now()) || dateTo.isBefore(dateFrom) || dateTo.isEqual(dateFrom)) {
            throw new DateParamErrorException();
        }
    }

    public List<Room> findMatchingAvailableRooms(RoomSearchParams roomSearchParams) {
        validateDates(roomSearchParams.period.getDateFrom(), roomSearchParams.period.getDateTo());
        List<Room> matchingRooms = roomRepository.findMatchingRooms(
                roomSearchParams.city,
                roomSearchParams.priceFrom,
                roomSearchParams.priceTo);
        if (roomSearchParams.priceFrom > roomSearchParams.priceTo) {
            throw new WrongPriceRangeException();
        }

        Set<Room> notAvailableRooms = reservationRepository.findAllByReservationStatusAndPeriodDateToAfterAndPeriodDateFromBefore(ReservationStatus.ACTIVE, roomSearchParams.period.getDateFrom(), roomSearchParams.period.getDateTo())
                .stream()
                .map(Reservation::getRoom)
                .collect(Collectors.toSet());
        matchingRooms.removeAll(notAvailableRooms);
        return matchingRooms;
    }
}
