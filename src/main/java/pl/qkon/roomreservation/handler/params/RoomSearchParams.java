package pl.qkon.roomreservation.handler.params;

import lombok.AllArgsConstructor;
import lombok.Builder;
import pl.qkon.roomreservation.dictionary.City;
import pl.qkon.roomreservation.embeddable.Period;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Builder
public class RoomSearchParams {

    @NotNull
    public final City city;
    @NotNull
    public final Period period;
    @NotNull
    public final Double priceFrom;
    @NotNull
    public final Double priceTo;
}
