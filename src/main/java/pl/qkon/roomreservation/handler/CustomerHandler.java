package pl.qkon.roomreservation.handler;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.qkon.roomreservation.entity.Customer;
import pl.qkon.roomreservation.repository.CustomerRepository;

import java.util.List;
import java.util.Optional;

@Component
@Transactional(readOnly = true)
public class CustomerHandler {
    private final CustomerRepository customerRepository;

    public CustomerHandler(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> findCustomers() {
        return customerRepository.findAll();
    }

    public Optional<Customer> findCustomer(Long id) {
        return customerRepository.findById(id);
    }
}
