package pl.qkon.roomreservation.handler;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.qkon.roomreservation.entity.Reservation;
import pl.qkon.roomreservation.repository.ReservationRepository;

import java.util.List;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class ReservationHandler {
    private final ReservationRepository reservationRepository;

    public List<Reservation> findUserReservations(Long userId) {
        return reservationRepository.findAllByCustomerId(userId);
    }
}
