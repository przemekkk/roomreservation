package pl.qkon.roomreservation.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.qkon.roomreservation.dictionary.ReservationStatus;
import pl.qkon.roomreservation.embeddable.Period;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    Customer customer;
    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false)
    Room room;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    ReservationStatus reservationStatus;
    @Column(nullable = false)
    Double price;
    @Embedded
    Period period;
}
