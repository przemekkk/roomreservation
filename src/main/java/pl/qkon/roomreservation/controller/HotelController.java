package pl.qkon.roomreservation.controller;

import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.qkon.roomreservation.dictionary.City;
import pl.qkon.roomreservation.embeddable.Period;
import pl.qkon.roomreservation.entity.Room;
import pl.qkon.roomreservation.handler.RoomSearchHandler;
import pl.qkon.roomreservation.handler.params.RoomSearchParams;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/room")
@AllArgsConstructor
public class HotelController {

    private final RoomSearchHandler roomSearchHandler;

    @GetMapping
    @ResponseBody
    public List<Room> findRoom(@RequestParam City city,
                               @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom,
                               @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo,
                               @RequestParam Double priceFrom,
                               @RequestParam Double priceTo) {
        RoomSearchParams roomSearchParams = RoomSearchParams
                .builder()
                .city(city)
                .period(new Period(dateFrom, dateTo))
                .priceFrom(priceFrom)
                .priceTo(priceTo)
                .build();

        return roomSearchHandler.findMatchingAvailableRooms(roomSearchParams);
    }

}
