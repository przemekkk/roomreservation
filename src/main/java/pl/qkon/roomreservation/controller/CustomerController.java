package pl.qkon.roomreservation.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.qkon.roomreservation.entity.Customer;
import pl.qkon.roomreservation.exception.NoCustomerExistsException;
import pl.qkon.roomreservation.handler.CustomerHandler;
import pl.qkon.roomreservation.processor.CustomerProcessor;
import pl.qkon.roomreservation.processor.command.RegisterCustomerCommand;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
@AllArgsConstructor
public class CustomerController {

    private final CustomerHandler customerHandler;
    private final CustomerProcessor customerProcessor;

    @GetMapping
    @ResponseBody
    public List<Customer> get() {
        return customerHandler.findCustomers();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Customer get(@PathVariable(name = "id") Long id) {
        return customerHandler.findCustomer(id).orElseThrow(() -> new NoCustomerExistsException(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Customer post(@RequestBody RegisterCustomerCommand command) {
        return customerProcessor.registerCustomer(command);
    }
}
