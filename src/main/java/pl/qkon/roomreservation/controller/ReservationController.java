package pl.qkon.roomreservation.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.qkon.roomreservation.entity.Reservation;
import pl.qkon.roomreservation.handler.ReservationHandler;
import pl.qkon.roomreservation.processor.ReservationProcessor;
import pl.qkon.roomreservation.processor.command.CancelReservationCommand;
import pl.qkon.roomreservation.processor.command.MakeReservationCommand;

import java.util.List;

@RestController
@RequestMapping("/api/reservation")
@AllArgsConstructor
public class ReservationController {

    private final ReservationHandler reservationHandler;
    private final ReservationProcessor reservationProcessor;

    @GetMapping
    @ResponseBody
    public List<Reservation> findUserReservations(Long userId) {
        return reservationHandler.findUserReservations(userId);
    }

    @PostMapping
    @ResponseBody
    public Reservation makeReservation(@RequestBody MakeReservationCommand command) {
        return reservationProcessor.makeReservation(command);
    }

    @PutMapping
    @ResponseBody
    public Reservation cancelReservation(@RequestBody CancelReservationCommand command) {
        return reservationProcessor.cancelReservation(command);
    }
}
