package pl.qkon.roomreservation.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = "pl.qkon.roomreservation.entity")
@EnableJpaRepositories(basePackages = "pl.qkon.roomreservation.repository")
@ComponentScan
public class RoomReservationConfiguration {
}
