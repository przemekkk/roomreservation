package pl.qkon.roomreservation.dictionary;

public enum ReservationStatus {
    ACTIVE, CANCELLED
}
