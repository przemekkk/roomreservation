package pl.qkon.roomreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.qkon.roomreservation.dictionary.ReservationStatus;
import pl.qkon.roomreservation.entity.Reservation;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findAllByCustomerId(Long id);


    @Query(value = "SELECT CASE WHEN count(r) > 0 THEN true ELSE false END FROM Reservation r " +
            "where r.room.id = ?1 AND r.reservationStatus = ?2  " +
            "AND r.period.dateTo > ?3 and  r.period.dateFrom < ?4 "
    )
    boolean existsByRoomIdAndReservationStatusAndPeriodDateToAfterAndPeriodDateFromBefore(Long roomId, ReservationStatus reservationStatus, LocalDate dateFrom, LocalDate dateTo);

    @Query(value = "SELECT r FROM Reservation r " +
            "where  r.reservationStatus = ?1 " +
            "AND r.period.dateTo > ?2 and  r.period.dateFrom < ?3 "
    )
    Set<Reservation> findAllByReservationStatusAndPeriodDateToAfterAndPeriodDateFromBefore(ReservationStatus reservationStatus, LocalDate dateFrom, LocalDate dateTo);
}
