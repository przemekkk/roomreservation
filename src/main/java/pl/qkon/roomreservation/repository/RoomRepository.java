package pl.qkon.roomreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.qkon.roomreservation.dictionary.City;
import pl.qkon.roomreservation.entity.Room;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Long> {

    @Query(value = "SELECT r FROM Room r " +
            "left join r.hotel h " +
            "where  h.city = ?1 " +
            "AND r.price between ?2 and ?3 "
    )
    List<Room> findMatchingRooms(City city, Double priceFrom, Double priceTo);


}
