package pl.qkon.roomreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.qkon.roomreservation.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
