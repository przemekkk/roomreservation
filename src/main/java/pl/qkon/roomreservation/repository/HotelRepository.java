package pl.qkon.roomreservation.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.qkon.roomreservation.entity.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, String> {
}
