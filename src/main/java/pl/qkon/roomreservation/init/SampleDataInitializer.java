package pl.qkon.roomreservation.init;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.qkon.roomreservation.dictionary.City;
import pl.qkon.roomreservation.entity.Hotel;
import pl.qkon.roomreservation.entity.Room;
import pl.qkon.roomreservation.processor.CustomerProcessor;
import pl.qkon.roomreservation.processor.command.RegisterCustomerCommand;
import pl.qkon.roomreservation.repository.HotelRepository;
import pl.qkon.roomreservation.repository.RoomRepository;

import java.util.Random;

@Component
@AllArgsConstructor
@Profile("dev")
public class SampleDataInitializer implements CommandLineRunner {

    private static final int ROOM_COUNT_PER_HOTEL = 10;
    public static final String MARRIOTT_WARSAW = "Marriott Warsaw";
    private final HotelRepository hotelRepository;
    private final RoomRepository roomRepository;
    private final CustomerProcessor customerProcessor;

    @Override
    public void run(String... args) {
        if (!hotelRepository.findById(MARRIOTT_WARSAW).isPresent()) {
            customerProcessor.registerCustomer(RegisterCustomerCommand.builder()
                    .name("Przemek")
                    .lastName("Konstanczuk")
                    .build());
            createHotel(MARRIOTT_WARSAW, City.WARSAW);
            createHotel("Marriott London", City.LONDON);
        }
    }

    private Hotel createHotel(String name, City city) {
        Random random = new Random();
        Hotel hotel = Hotel.builder()
                .name(name)
                .city(city)
                .build();
        hotelRepository.save(hotel);
        for (int i = 0; i < ROOM_COUNT_PER_HOTEL; i++) {
            Room room = Room.builder()
                    .hotel(hotel)
                    .price(random.nextInt(50) + 50.00)
                    .build();
            roomRepository.save(room);
        }
        return hotel;
    }
}
