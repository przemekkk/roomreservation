package pl.qkon.roomreservation.exception;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@AllArgsConstructor
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No room with given id exists")
public class NoRoomFoundException extends RuntimeException {
    public final Long id;
}
