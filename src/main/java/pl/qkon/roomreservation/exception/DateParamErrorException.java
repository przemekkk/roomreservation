package pl.qkon.roomreservation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Dates needs to be in the future and date from needs to be before date after")
public class DateParamErrorException extends RuntimeException {
}
