package pl.qkon.roomreservation.exception;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@AllArgsConstructor
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "The room already reserved")
public class RoomAlreadyReservedException extends RuntimeException {
    public final Long id;
}
