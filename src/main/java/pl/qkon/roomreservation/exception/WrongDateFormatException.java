package pl.qkon.roomreservation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Date needs to be in format yyyy-MM-dd")
public class WrongDateFormatException extends RuntimeException {
}
