package pl.qkon.roomreservation.processor.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Builder
//Needed for Swagger UI to show properly
@Getter
public class RegisterCustomerCommand {
    @NotNull
    public String name;
    @NotNull
    public String lastName;
}
