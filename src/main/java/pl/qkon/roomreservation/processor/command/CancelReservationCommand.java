package pl.qkon.roomreservation.processor.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
//Needed for Swagger UI to show properly
@Getter
@NoArgsConstructor
@Builder
public class CancelReservationCommand {

    @NotNull
    public Long userId;
    @NotNull
    public Long reservationId;
}
