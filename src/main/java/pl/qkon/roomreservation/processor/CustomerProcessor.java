package pl.qkon.roomreservation.processor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.qkon.roomreservation.entity.Customer;
import pl.qkon.roomreservation.processor.command.RegisterCustomerCommand;
import pl.qkon.roomreservation.repository.CustomerRepository;

@Service
public class CustomerProcessor {

    private final CustomerRepository customerRepository;

    public CustomerProcessor(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Transactional
    public Customer registerCustomer(RegisterCustomerCommand command) {
        return customerRepository.save(Customer.builder()
                .name(command.getName())
                .lastName(command.getLastName())
                .build());
    }
}
