package pl.qkon.roomreservation.processor;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.qkon.roomreservation.dictionary.ReservationStatus;
import pl.qkon.roomreservation.embeddable.Period;
import pl.qkon.roomreservation.entity.Customer;
import pl.qkon.roomreservation.entity.Reservation;
import pl.qkon.roomreservation.entity.Room;
import pl.qkon.roomreservation.exception.NoCustomerExistsException;
import pl.qkon.roomreservation.exception.NoReservationFoundException;
import pl.qkon.roomreservation.exception.NoRoomFoundException;
import pl.qkon.roomreservation.exception.RoomAlreadyReservedException;
import pl.qkon.roomreservation.exception.WrongDateFormatException;
import pl.qkon.roomreservation.handler.RoomSearchHandler;
import pl.qkon.roomreservation.processor.command.CancelReservationCommand;
import pl.qkon.roomreservation.processor.command.MakeReservationCommand;
import pl.qkon.roomreservation.repository.CustomerRepository;
import pl.qkon.roomreservation.repository.ReservationRepository;
import pl.qkon.roomreservation.repository.RoomRepository;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@AllArgsConstructor
public class ReservationProcessor {

    private final ReservationRepository reservationRepository;
    private final CustomerRepository customerRepository;
    private final RoomRepository roomRepository;


    @Transactional
    public Reservation makeReservation(MakeReservationCommand makeReservationCommand) {
        LocalDate dateFrom;
        LocalDate dateTo;
        try {
            dateFrom = LocalDate.parse(makeReservationCommand.dateFrom);
            dateTo = LocalDate.parse(makeReservationCommand.dateTo);
        } catch (Exception e) {
            throw new WrongDateFormatException();
        }
        RoomSearchHandler.validateDates(dateFrom, dateTo);
        Customer customer = customerRepository.findById(makeReservationCommand.userId)
                .orElseThrow(() -> new NoCustomerExistsException(makeReservationCommand.userId));
        Room room = roomRepository.findById(makeReservationCommand.roomId)
                .orElseThrow(() -> new NoRoomFoundException(makeReservationCommand.roomId));

        if (reservationRepository.existsByRoomIdAndReservationStatusAndPeriodDateToAfterAndPeriodDateFromBefore(makeReservationCommand.roomId, ReservationStatus.ACTIVE, dateFrom, dateTo)) {
            throw new RoomAlreadyReservedException(makeReservationCommand.roomId);
        }

        Reservation reservation = Reservation.builder()
                .customer(customer)
                .period(new Period(dateFrom, dateTo))
                .reservationStatus(ReservationStatus.ACTIVE)
                .room(room)
                .price(DAYS.between(dateFrom, dateTo) * room.getPrice())
                .build();
        return reservationRepository.save(reservation);

    }

    @Transactional
    public Reservation cancelReservation(CancelReservationCommand cancelReservationCommand) {
        Reservation reservation = reservationRepository.findById(cancelReservationCommand.reservationId)
                .filter(res -> res.getCustomer().getId().equals(cancelReservationCommand.userId))
                .orElseThrow(() -> new NoReservationFoundException(cancelReservationCommand.reservationId, cancelReservationCommand.userId));

        reservation.setReservationStatus(ReservationStatus.CANCELLED);
        return reservationRepository.save(reservation);

    }
}
